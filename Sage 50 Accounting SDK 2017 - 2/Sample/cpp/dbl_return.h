/* ======================================================================================
 * dbl_return.h
 *
 * This file contains defines for the DBS return codes.
 *
 * Copyright (c) 1999-2014 Sage Software Canada, Ltd. All Rights Reserved.
 * ======================================================================================
 */

#define DBS_SUCCESS                 0

#define DBS_DD_INCONSISTENT         1
#define DBS_BAD_DATABASE            2
#define DBS_CREATE_ERROR            3
#define DBS_DROP_ERROR              4

#define DBS_DATABASE_VERSION        97
#define DBS_CONNECT_ERROR           98
#define DBS_DATABASE_CONFIG         99
#define DBS_BAD_DATABASE_SPEC       100
#define DBS_DATABASE_NOT_STARTED    101

#define DBS_TOO_MANY_LINKS          104
#define DBS_INVALID_LINK_TYPE       105
#define DBS_INVALID_LINK            106
#define DBS_MEMORY                  107
#define DBS_DISK_FULL               108
#define DBS_NOT_ALLOWED             109
#define DBS_INVALID_HANDLE          110
#define DBS_INVALID_FLAGS           111
#define DBS_TOO_MANY_OPEN_TABLES    112
#define DBS_ACCESS_DENIED           113
#define DBS_BUFFER_SIZE             114
#define DBS_NOT_IN_TRANSACTION      115
#define DBS_ACTIVE_TRANSACTION      116

#define DBS_LOCKED                  118
#define DBS_NOT_FOUND               119
#define DBS_DEADLOCK                120
#define DBS_NO_MORE_DATA            121
#define DBS_INVALID_POSITION        122
#define DBS_CONFLICT                123
#define DBS_RECORD_NOT_LOCKED       124
#define DBS_KEY_CHANGED             125
#define DBS_NOT_CURRENT_RECORD      126
#define DBS_DUPLICATE_KEY           127
#define DBS_ACCESSDENIED            128
#define DBS_BAD_USERID              129
#define DBS_BAD_PW                  130
#define DBS_NOT_SUPPORTED           131
#define DBS_TOO_MANY_LOCKS          132
#define DBS_TABLE_NOT_FOUND         133
#define DBS_TABLE_EXISTS            134

#define DBS_INVALID_KEY_NUMBER      200
#define DBS_TOO_MANY_KEYS           201
#define DBS_TOO_MANY_KEY_SEGMENTS   202
#define DBS_INVALID_KEY_LENGTH      203
#define DBS_INVALID_FIELD_DECIMALS  204
#define DBS_INVALID_FIELD_NUMBER    205
#define DBS_INVALID_FIELD_TYPE      206
#define DBS_INVALID_FIELD_OFFSET    207
#define DBS_INVALID_FIELD_LENGTH    208
#define DBS_INVALID_RECORD_LENGTH   209
#define DBS_INVALID_TABLE_NAME      210
#define DBS_INVALID_FIELD_NAME      211
#define DBS_INVALID_PRIMARY_KEY     212
#define DBS_INVALID_AUDITSTAMP      213
#define DBS_NOT_MP_RECORD           214
#define DBS_BAD_KEY_SEGMENT         215
#define DBS_TOO_MANY_FIELDS         216
#define DBS_NO_FIELDS_DEFINED       217
#define DBS_NO_KEYS_DEFINED         218
#define DBS_BAD_KEY_FLAGS           219
#define DBS_BAD_KEY_FLDNO           220
#define DBS_BAD_KEY_FLDTYPE         221
#define DBS_DUPLICATE_FIELD_NAME    222
#define DBS_FIELD_NOT_FOUND         223
#define DBS_KEY_ALREADY_DEFINED     224
#define DBS_CORRUPT_DATABASE        225

#define DBS_FL_INVALID_FIELD        300
#define DBS_FL_INVALID_OPERATOR     301
#define DBS_FL_INVALID_BOOLEAN      302
#define DBS_FL_BRACKET_MISMATCH     303
#define DBS_FL_INVALID_BRACKET      304
#define DBS_FL_BAD_CONSTANT         305
#define DBS_FL_STACK_OVERFLOW       306
#define DBS_FL_STACK_UNDERFLOW      307
#define DBS_FL_EVAL_ERROR           308
#define DBS_FL_TOO_BIG              309

#define DBS_DRIVER_NOT_DEFINED      0xA000u
#define DBS_BAD_DRIVER_SPEC         0xA001u
#define DBS_DRIVER_EXEC             0xA002u
#define DBS_DRIVER_INIT             0xA003u

#define DBS_NOT_MAPPED              0xC001u
