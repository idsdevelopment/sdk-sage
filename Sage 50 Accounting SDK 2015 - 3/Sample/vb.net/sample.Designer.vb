<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmSample
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtPassword As System.Windows.Forms.TextBox
	Public WithEvents txtUserName As System.Windows.Forms.TextBox
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents txtFileName As System.Windows.Forms.TextBox
	Public WithEvents lblPassword As System.Windows.Forms.Label
	Public WithEvents lblUserName As System.Windows.Forms.Label
	Public WithEvents lblNumProjects As System.Windows.Forms.Label
	Public WithEvents lblNumInventory As System.Windows.Forms.Label
	Public WithEvents lblNumEmployees As System.Windows.Forms.Label
	Public WithEvents lblNumCustomers As System.Windows.Forms.Label
	Public WithEvents lblNumVendors As System.Windows.Forms.Label
	Public WithEvents lblNumAccounts As System.Windows.Forms.Label
	Public WithEvents lblProject As System.Windows.Forms.Label
	Public WithEvents lblInventory As System.Windows.Forms.Label
	Public WithEvents lblEmployees As System.Windows.Forms.Label
	Public WithEvents lblCustomers As System.Windows.Forms.Label
	Public WithEvents lblVendors As System.Windows.Forms.Label
	Public WithEvents lblAccounts As System.Windows.Forms.Label
	Public WithEvents lblRecsonFile As System.Windows.Forms.Label
	Public WithEvents lblCompanyName As System.Windows.Forms.Label
	Public WithEvents Line1 As System.Windows.Forms.Label
	Public WithEvents lblFileName As System.Windows.Forms.Label
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtPassword = New System.Windows.Forms.TextBox
        Me.txtUserName = New System.Windows.Forms.TextBox
        Me.cmdOK = New System.Windows.Forms.Button
        Me.txtFileName = New System.Windows.Forms.TextBox
        Me.lblPassword = New System.Windows.Forms.Label
        Me.lblUserName = New System.Windows.Forms.Label
        Me.lblNumProjects = New System.Windows.Forms.Label
        Me.lblNumInventory = New System.Windows.Forms.Label
        Me.lblNumEmployees = New System.Windows.Forms.Label
        Me.lblNumCustomers = New System.Windows.Forms.Label
        Me.lblNumVendors = New System.Windows.Forms.Label
        Me.lblNumAccounts = New System.Windows.Forms.Label
        Me.lblProject = New System.Windows.Forms.Label
        Me.lblInventory = New System.Windows.Forms.Label
        Me.lblEmployees = New System.Windows.Forms.Label
        Me.lblCustomers = New System.Windows.Forms.Label
        Me.lblVendors = New System.Windows.Forms.Label
        Me.lblAccounts = New System.Windows.Forms.Label
        Me.lblRecsonFile = New System.Windows.Forms.Label
        Me.lblCompanyName = New System.Windows.Forms.Label
        Me.Line1 = New System.Windows.Forms.Label
        Me.lblFileName = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'txtPassword
        '
        Me.txtPassword.AcceptsReturn = True
        Me.txtPassword.BackColor = System.Drawing.SystemColors.Window
        Me.txtPassword.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPassword.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPassword.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPassword.ImeMode = System.Windows.Forms.ImeMode.Disable
        Me.txtPassword.Location = New System.Drawing.Point(96, 64)
        Me.txtPassword.MaxLength = 0
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPassword.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPassword.Size = New System.Drawing.Size(137, 19)
        Me.txtPassword.TabIndex = 2
        '
        'txtUserName
        '
        Me.txtUserName.AcceptsReturn = True
        Me.txtUserName.BackColor = System.Drawing.SystemColors.Window
        Me.txtUserName.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtUserName.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUserName.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtUserName.Location = New System.Drawing.Point(96, 40)
        Me.txtUserName.MaxLength = 0
        Me.txtUserName.Name = "txtUserName"
        Me.txtUserName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtUserName.Size = New System.Drawing.Size(137, 19)
        Me.txtUserName.TabIndex = 1
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(296, 56)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(73, 25)
        Me.cmdOK.TabIndex = 4
        Me.cmdOK.Text = "Open"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'txtFileName
        '
        Me.txtFileName.AcceptsReturn = True
        Me.txtFileName.BackColor = System.Drawing.SystemColors.Window
        Me.txtFileName.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtFileName.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFileName.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtFileName.Location = New System.Drawing.Point(96, 16)
        Me.txtFileName.MaxLength = 0
        Me.txtFileName.Name = "txtFileName"
        Me.txtFileName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtFileName.Size = New System.Drawing.Size(273, 19)
        Me.txtFileName.TabIndex = 0
        '
        'lblPassword
        '
        Me.lblPassword.BackColor = System.Drawing.SystemColors.Control
        Me.lblPassword.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPassword.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPassword.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPassword.Location = New System.Drawing.Point(8, 64)
        Me.lblPassword.Name = "lblPassword"
        Me.lblPassword.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPassword.Size = New System.Drawing.Size(57, 17)
        Me.lblPassword.TabIndex = 20
        Me.lblPassword.Text = "Password:"
        '
        'lblUserName
        '
        Me.lblUserName.BackColor = System.Drawing.SystemColors.Control
        Me.lblUserName.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblUserName.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUserName.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblUserName.Location = New System.Drawing.Point(8, 40)
        Me.lblUserName.Name = "lblUserName"
        Me.lblUserName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblUserName.Size = New System.Drawing.Size(82, 17)
        Me.lblUserName.TabIndex = 19
        Me.lblUserName.Text = "User Name:"
        '
        'lblNumProjects
        '
        Me.lblNumProjects.BackColor = System.Drawing.SystemColors.Control
        Me.lblNumProjects.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblNumProjects.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNumProjects.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblNumProjects.Location = New System.Drawing.Point(304, 200)
        Me.lblNumProjects.Name = "lblNumProjects"
        Me.lblNumProjects.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblNumProjects.Size = New System.Drawing.Size(67, 17)
        Me.lblNumProjects.TabIndex = 18
        '
        'lblNumInventory
        '
        Me.lblNumInventory.BackColor = System.Drawing.SystemColors.Control
        Me.lblNumInventory.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblNumInventory.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNumInventory.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblNumInventory.Location = New System.Drawing.Point(304, 176)
        Me.lblNumInventory.Name = "lblNumInventory"
        Me.lblNumInventory.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblNumInventory.Size = New System.Drawing.Size(67, 17)
        Me.lblNumInventory.TabIndex = 17
        '
        'lblNumEmployees
        '
        Me.lblNumEmployees.BackColor = System.Drawing.SystemColors.Control
        Me.lblNumEmployees.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblNumEmployees.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNumEmployees.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblNumEmployees.Location = New System.Drawing.Point(304, 152)
        Me.lblNumEmployees.Name = "lblNumEmployees"
        Me.lblNumEmployees.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblNumEmployees.Size = New System.Drawing.Size(67, 17)
        Me.lblNumEmployees.TabIndex = 16
        '
        'lblNumCustomers
        '
        Me.lblNumCustomers.BackColor = System.Drawing.SystemColors.Control
        Me.lblNumCustomers.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblNumCustomers.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNumCustomers.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblNumCustomers.Location = New System.Drawing.Point(112, 200)
        Me.lblNumCustomers.Name = "lblNumCustomers"
        Me.lblNumCustomers.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblNumCustomers.Size = New System.Drawing.Size(67, 17)
        Me.lblNumCustomers.TabIndex = 15
        '
        'lblNumVendors
        '
        Me.lblNumVendors.BackColor = System.Drawing.SystemColors.Control
        Me.lblNumVendors.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblNumVendors.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNumVendors.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblNumVendors.Location = New System.Drawing.Point(112, 176)
        Me.lblNumVendors.Name = "lblNumVendors"
        Me.lblNumVendors.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblNumVendors.Size = New System.Drawing.Size(67, 17)
        Me.lblNumVendors.TabIndex = 14
        '
        'lblNumAccounts
        '
        Me.lblNumAccounts.BackColor = System.Drawing.SystemColors.Control
        Me.lblNumAccounts.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblNumAccounts.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNumAccounts.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblNumAccounts.Location = New System.Drawing.Point(112, 152)
        Me.lblNumAccounts.Name = "lblNumAccounts"
        Me.lblNumAccounts.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblNumAccounts.Size = New System.Drawing.Size(67, 17)
        Me.lblNumAccounts.TabIndex = 13
        '
        'lblProject
        '
        Me.lblProject.BackColor = System.Drawing.SystemColors.Control
        Me.lblProject.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblProject.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProject.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblProject.Location = New System.Drawing.Point(208, 200)
        Me.lblProject.Name = "lblProject"
        Me.lblProject.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblProject.Size = New System.Drawing.Size(89, 17)
        Me.lblProject.TabIndex = 12
        Me.lblProject.Text = "Project:"
        '
        'lblInventory
        '
        Me.lblInventory.BackColor = System.Drawing.SystemColors.Control
        Me.lblInventory.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblInventory.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInventory.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblInventory.Location = New System.Drawing.Point(208, 176)
        Me.lblInventory.Name = "lblInventory"
        Me.lblInventory.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblInventory.Size = New System.Drawing.Size(89, 17)
        Me.lblInventory.TabIndex = 11
        Me.lblInventory.Text = "Inventory:"
        '
        'lblEmployees
        '
        Me.lblEmployees.BackColor = System.Drawing.SystemColors.Control
        Me.lblEmployees.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblEmployees.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployees.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblEmployees.Location = New System.Drawing.Point(208, 152)
        Me.lblEmployees.Name = "lblEmployees"
        Me.lblEmployees.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblEmployees.Size = New System.Drawing.Size(89, 17)
        Me.lblEmployees.TabIndex = 10
        Me.lblEmployees.Text = "Employees:"
        '
        'lblCustomers
        '
        Me.lblCustomers.BackColor = System.Drawing.SystemColors.Control
        Me.lblCustomers.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCustomers.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCustomers.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCustomers.Location = New System.Drawing.Point(16, 200)
        Me.lblCustomers.Name = "lblCustomers"
        Me.lblCustomers.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCustomers.Size = New System.Drawing.Size(89, 17)
        Me.lblCustomers.TabIndex = 9
        Me.lblCustomers.Text = "Customers:"
        '
        'lblVendors
        '
        Me.lblVendors.BackColor = System.Drawing.SystemColors.Control
        Me.lblVendors.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVendors.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVendors.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVendors.Location = New System.Drawing.Point(16, 176)
        Me.lblVendors.Name = "lblVendors"
        Me.lblVendors.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVendors.Size = New System.Drawing.Size(89, 17)
        Me.lblVendors.TabIndex = 8
        Me.lblVendors.Text = "Vendors:"
        '
        'lblAccounts
        '
        Me.lblAccounts.BackColor = System.Drawing.SystemColors.Control
        Me.lblAccounts.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAccounts.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAccounts.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAccounts.Location = New System.Drawing.Point(16, 152)
        Me.lblAccounts.Name = "lblAccounts"
        Me.lblAccounts.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAccounts.Size = New System.Drawing.Size(89, 17)
        Me.lblAccounts.TabIndex = 7
        Me.lblAccounts.Text = "Accounts:"
        '
        'lblRecsonFile
        '
        Me.lblRecsonFile.BackColor = System.Drawing.SystemColors.Control
        Me.lblRecsonFile.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblRecsonFile.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRecsonFile.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblRecsonFile.Location = New System.Drawing.Point(8, 128)
        Me.lblRecsonFile.Name = "lblRecsonFile"
        Me.lblRecsonFile.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblRecsonFile.Size = New System.Drawing.Size(121, 17)
        Me.lblRecsonFile.TabIndex = 6
        Me.lblRecsonFile.Text = "Records on file:"
        '
        'lblCompanyName
        '
        Me.lblCompanyName.BackColor = System.Drawing.SystemColors.Control
        Me.lblCompanyName.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCompanyName.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompanyName.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCompanyName.Location = New System.Drawing.Point(8, 96)
        Me.lblCompanyName.Name = "lblCompanyName"
        Me.lblCompanyName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCompanyName.Size = New System.Drawing.Size(353, 17)
        Me.lblCompanyName.TabIndex = 5
        '
        'Line1
        '
        Me.Line1.BackColor = System.Drawing.SystemColors.WindowText
        Me.Line1.Location = New System.Drawing.Point(8, 88)
        Me.Line1.Name = "Line1"
        Me.Line1.Size = New System.Drawing.Size(368, 1)
        Me.Line1.TabIndex = 21
        '
        'lblFileName
        '
        Me.lblFileName.BackColor = System.Drawing.SystemColors.Control
        Me.lblFileName.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFileName.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFileName.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblFileName.Location = New System.Drawing.Point(8, 16)
        Me.lblFileName.Name = "lblFileName"
        Me.lblFileName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFileName.Size = New System.Drawing.Size(97, 17)
        Me.lblFileName.TabIndex = 3
        Me.lblFileName.Text = "Enter File Name:"
        '
        'frmSample
        '
        Me.AcceptButton = Me.cmdOK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(380, 235)
        Me.Controls.Add(Me.txtPassword)
        Me.Controls.Add(Me.txtUserName)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.txtFileName)
        Me.Controls.Add(Me.lblPassword)
        Me.Controls.Add(Me.lblUserName)
        Me.Controls.Add(Me.lblNumProjects)
        Me.Controls.Add(Me.lblNumInventory)
        Me.Controls.Add(Me.lblNumEmployees)
        Me.Controls.Add(Me.lblNumCustomers)
        Me.Controls.Add(Me.lblNumVendors)
        Me.Controls.Add(Me.lblNumAccounts)
        Me.Controls.Add(Me.lblProject)
        Me.Controls.Add(Me.lblInventory)
        Me.Controls.Add(Me.lblEmployees)
        Me.Controls.Add(Me.lblCustomers)
        Me.Controls.Add(Me.lblVendors)
        Me.Controls.Add(Me.lblAccounts)
        Me.Controls.Add(Me.lblRecsonFile)
        Me.Controls.Add(Me.lblCompanyName)
        Me.Controls.Add(Me.Line1)
        Me.Controls.Add(Me.lblFileName)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Location = New System.Drawing.Point(4, 23)
        Me.Name = "frmSample"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "Sample"
        Me.ResumeLayout(False)

    End Sub
#End Region 
End Class