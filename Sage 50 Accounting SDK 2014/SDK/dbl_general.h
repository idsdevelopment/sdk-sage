/* ======================================================================================
 * dbl_general.h
 *
 * This file contains exports for the general database routines.
 *
 * Copyright (c) 1999-2013 Sage Software Canada, Ltd. All Rights Reserved.
 * ======================================================================================
 */



//database linking routines
extern WORD WINAPI wSDBOpenDatabase(LPWORD pDBLinkNo, WORD wFlags, LPSTR pszAppID, LPSTR pszUserID, LPSTR pszPwd, LPSTR szServerId, LPSTR szCompId, LPSTR szPort);
extern WORD WINAPI wSDBCloseDatabase(WORD wDBLinkNo);
extern WORD WINAPI wSDBGetDBVer(WORD wDBLinkNo, short* pFileVer, short* pCtyCode, short *pDBCode, bool *pbIsAccountantCopyDB);

//table maintenance routines
extern WORD WINAPI wSDBOpenTable(WORD wDBLinkNo, LPTSTR pszTableName, WORD wFlags, LPWORD pTBLinkNo);
extern WORD WINAPI wSDBEmptyTable(WORD wDBLinkNo, WORD wTBLinkNo);
extern WORD WINAPI wSDBCloseTable(WORD wDBLinkNo, WORD wTBLinkNo);
extern bool WINAPI bSDBHasRecs(WORD wDBLinkNo, WORD wTBLinkNo);
extern WORD WINAPI wSDBGetNumRecs(WORD wDBLinkNo, WORD wTBLinkNo, LPLONG pNumRecs);

//record access routines
extern WORD WINAPI wSDBGetSetFWD(WORD wDBLinkNo, WORD wTBLinkNo, WORD wKey, LPVOID pRecord, WORD wFilterSeg, WORD wExpRecs, BOOL bLock);
extern WORD WINAPI wSDBGetSetREV(WORD wDBLinkNo, WORD wTBLinkNo, WORD wKey, LPVOID pRecord, WORD wFilterSeg, WORD wExpRecs, BOOL bLock);
extern WORD WINAPI wSDBGetNextRecord(WORD wDBLinkNo, WORD wTBLinkNo, LPVOID pRecord, BOOL bLock);
extern WORD WINAPI wSDBGetPrevRecord(WORD wDBLinkNo, WORD wTBLinkNo, LPVOID pRecord, BOOL bLock);
extern WORD WINAPI wSDBGetRecord(WORD wDBLinkNo, WORD wTBLinkNo, WORD wKey, LPVOID pRecord);
extern WORD WINAPI wSDBGetLock(WORD wDBLinkNo, WORD wTBLinkNo, WORD wKey, LPVOID pRecord, BOOL bConcCheck);
extern WORD WINAPI wSDBUnlockRecord(WORD wDBLinkNo, WORD wTBLinkNo, LPVOID pRecord);
extern WORD WINAPI wSDBIsRecLocked(WORD wDBLinkNo, WORD wTBLinkNo);

//record updating routines
extern WORD WINAPI wSDBInsertRecord(WORD wDBLinkNo, WORD wTBLinkNo, LPVOID pRecord);
extern WORD WINAPI wSDBUpdateRecord(WORD wDBLinkNo, WORD wTBLinkNo, LPVOID pRecord);
extern WORD WINAPI wSDBDeleteRecord(WORD wDBLinkNo, WORD wTBLinkNo, LPVOID pRecord);

//record filter routines
extern WORD WINAPI wSDBSelectRecords(WORD wDBLinkNo, WORD wTBLinkNo, LPTSTR pszFilter, WORD wKey, BOOL bAscending);
extern WORD WINAPI wSDBFetchRecord(WORD wDBLinkNo, WORD wTBLinkNo, LPVOID pRecord, WORD wFetch, LPWORD pFetched);
extern WORD WINAPI wSDBExecSelectSQL(WORD wDBLinkNo, LPTSTR pszSqlSmt, LPWORD pTBLinkNo);
extern WORD WINAPI wSDBRemoveRecords(WORD wDBLinkNo, WORD wTBLinkNo, LPTSTR pszFilter, WORD wKey);

//transaction preocessing routines
extern WORD WINAPI wSDBTransBegin(WORD wDBLinkNo);
extern WORD WINAPI wSDBTransCommit(WORD wDBLinkNo);
extern WORD WINAPI wSDBTransRollback(WORD wDBLinkNo);

//miscellaneous routines
extern WORD WINAPI wSDBLastError(WORD wDBLinkNo, LPWORD pErrCode);
extern void WINAPI vSDBUnloadDAO();

//BCD manipulation routines
extern long   WINAPI lBcdToLong(LPBYTE pNumber, WORD nLength);
extern LPBYTE WINAPI pLongToBcd(LPBYTE pNumber, long lValue, WORD nLength);

