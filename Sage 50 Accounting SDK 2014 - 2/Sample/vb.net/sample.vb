Option Strict Off
Option Explicit On

Imports System.Runtime.InteropServices
Imports System.Text


Friend Class frmSample
	Inherits System.Windows.Forms.Form

   Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
      Dim lNum As Integer
      Dim lId As Integer
      Dim iRet As Short
      Dim pRecord(1999) As Byte
      Dim iDBLink As Short
      Dim iTBLink As Short
      Dim iFileVer As Short
      Dim iCtyCode As Short
      Dim iDBCode As Short
      Dim bIsAcctCopy As Boolean
      Dim szUserName As String
      Dim szPassword As String
      Dim szFileName As String
      Dim szCompName As String
      Dim sFindData As WIN32_FIND_DATA

      szFileName = Trim(txtFileName.Text)
      szUserName = Trim(txtUserName.Text)
      szPassword = Trim(txtPassword.Text)

      sFindData = New WIN32_FIND_DATA
      If szFileName = "" Or FindFirstFile(szFileName, sFindData) = -1 Then
         DisplayError(("The file does not exsit. Please select another file."))
         Exit Sub
      End If

      Dim szHost As String
      Dim szPort As String
      Dim szFileDir As String
      Dim lLength As Long
      Dim dbClient As New Simply.ConnectionManagerServiceClient.ConnectionManagerServiceClient
      Dim dbError As Simply.ConnectionManagerService.ConnectionManagerError

      szFileDir = LCase$(szFileName)
      lLength = Len(szFileDir)
      If (Strings.Right(szFileDir, 4) = ".sai") Then
         szFileDir = Strings.Left(szFileDir, lLength - 4) + Replace(Strings.Right(szFileDir, 4), ".sai", ".saj")
      End If

      szHost = ""
      szPort = ""
      dbError = dbClient.GetConnectionInfo(szFileDir, szHost, szPort)

      If dbError = Simply.ConnectionManagerService.ConnectionManagerError.No_Error Then

            If wSDBOpenDatabase(iDBLink, FLG_SHARED, "Sample", szUserName, szPassword, szHost, "", szPort) = DBS_SUCCESS Then
                If wSDBGetDBVer(iDBLink, iFileVer, iCtyCode, iDBCode, bIsAcctCopy) = DBS_SUCCESS Then
                    If (iFileVer < 21201) Then
                        DisplayError(("The SDK will only work with databases created in Sage 50 Accounting (Release 2014.2). Please select another file."))
                        Call wSDBCloseDatabase(iDBLink)
                        Call vSDBUnloadDAO()
                        Exit Sub
                    End If
                End If
                If wSDBOpenTable(iDBLink, "tCompany", FLG_READONLY, iTBLink) = DBS_SUCCESS Then
                    'Read record #1 in the company info record
                    lId = 1
                    Call SetBytes(pRecord, System.BitConverter.GetBytes(lId), 0, 4)
                    iRet = wSDBGetRecord(iDBLink, iTBLink, FLG_KEY0, pRecord)

                    szCompName = ""
                    If (iFileVer > 9000) Then
                        Call Byte2String(pRecord, szCompName, 34, 52)
                    Else
                        Call Byte2String(pRecord, szCompName, 28, 52)
                    End If
                    lblCompanyName.Text = Trim(szCompName)
                End If
                Call wSDBCloseTable(iDBLink, iTBLink)

                'Get the number of records per module
                lNum = GetNumRecModule(iDBLink, "tAccount")
                lblNumAccounts.Text = Trim(Str(lNum))
                lNum = GetNumRecModule(iDBLink, "tVendor")
                lblNumVendors.Text = Trim(Str(lNum))
                lNum = GetNumRecModule(iDBLink, "tCustomr")
                lblNumCustomers.Text = Trim(Str(lNum))
                lNum = GetNumRecModule(iDBLink, "tEmp")
                lblNumEmployees.Text = Trim(Str(lNum))
                lNum = GetNumRecModule(iDBLink, "tInvent")
                lblNumInventory.Text = Trim(Str(lNum))
                lNum = GetNumRecModule(iDBLink, "tProject")
                lblNumProjects.Text = Trim(Str(lNum))

                Call wSDBCloseDatabase(iDBLink)
            Else
                DisplayError(("Cannot open the database. Please select another file."))
            End If
         Call vSDBUnloadDAO()
      Else
         DisplayError(("Cannot connect to database server."))
      End If

   End Sub


   Private Sub Byte2String(ByRef ArrayOfBytes() As Byte, ByRef StringEqv As String, ByVal start As Short, ByVal strSize As Short)
      Dim i As Short
      StringEqv = ""
      For i = 0 To strSize - 1
         If ArrayOfBytes(start + i) = 0 Then
            Exit For
         Else
            StringEqv = StringEqv + Convert.ToChar(ArrayOfBytes(start + i))
         End If
      Next i
   End Sub


   Private Sub SetBytes(ByRef byteCopyto() As Byte, ByRef byteCopyFrom() As Byte, ByVal nStart As Short, ByVal nSize As Short)
      Dim n As Short
      For n = 0 To nSize - 1
         'Make sure we don't copy something we don't have.
         If n < byteCopyFrom.Length - 1 Then
            If byteCopyFrom(n) = 0 Then
               Exit For
            Else
               byteCopyto(nStart + n) = byteCopyFrom(n)
            End If
         End If
      Next n

   End Sub


   Private Sub DisplayError(ByRef szErrorString As String)
      Call MsgBox(szErrorString, MsgBoxStyle.ApplicationModal + MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly, "Sample - Error")
      lblCompanyName.Text = ""
      lblNumAccounts.Text = ""
      lblNumVendors.Text = ""
      lblNumCustomers.Text = ""
      lblNumInventory.Text = ""
      lblNumEmployees.Text = ""
      lblNumProjects.Text = ""
      txtUserName.Text = ""
      txtPassword.Text = ""
      txtFileName.Text = ""
      txtFileName.Focus()
   End Sub


   Private Function GetNumRecModule(ByRef iDBLink As Short, ByRef szTableName As String) As Integer
      Dim iTBLink As Short
      Dim lNum As Integer

      lNum = 0
      If wSDBOpenTable(iDBLink, szTableName, FLG_READONLY, iTBLink) = DBS_SUCCESS Then
         Call wSDBGetNumRecs(iDBLink, iTBLink, lNum)
         Call wSDBCloseTable(iDBLink, iTBLink)
      End If
      GetNumRecModule = lNum
   End Function


   Private Sub cmdClose_Click()
      Me.Close()
   End Sub
End Class