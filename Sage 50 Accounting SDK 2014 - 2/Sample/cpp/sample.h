/* ======================================================================================
 * sample.h
 *
 * Sample program using the Sage 50 Accounting SDK. The following program will open a
 * Sage 50 Accounting database and display the company information.
 *
 * Copyright (c) 1999-2013 Sage Software Canada, Ltd. All Rights Reserved.
 *
 * ======================================================================================
 */

#define FILENAME        1000
#define USERNAME        1001
#define PASSWORD        1002
#define COMPNAME        1003
#define NUMACCOUNTS     1004
#define NUMVENDORS      1005
#define NUMCUSTOMERS    1006
#define NUMEMPLOYEES    1007
#define NUMINVENTORY    1008
#define NUMPROJECTS     1009


// for backward compatibility
#if _WIN32_WINNT >= 0x0400
#define _CoInitialize()  CoInitializeEx(NULL, COINIT_APARTMENTTHREADED)
#else
#define _CoInitialize()  CoInitialize(NULL)
#endif

