/* ======================================================================================
 * sample.rc
 *
 * Sample program using the Sage 50 Accounting SDK. The following program will open a
 * Sage 50 Accounting database and display the company information.
 *
 * Copyright (c) 1999-2013 Sage Software Canada, Ltd. All Rights Reserved.
 *
 * ======================================================================================
 */
#include <windows.h>
#include "sample.h"


SAMPLEDLG DIALOG LOADONCALL MOVEABLE DISCARDABLE 50, 45, 200, 97
STYLE DS_MODALFRAME | WS_POPUP | WS_CAPTION | WS_SYSMENU
CAPTION "Sample"
FONT 8, "MS Sans Serif"
  BEGIN
    CONTROL "Enter File Name:",  -1,         "static", 
                                 SS_LEFTNOWORDWRAP | WS_CHILD | WS_CLIPCHILDREN | SS_NOPREFIX,  5, 10,  55,  8
    CONTROL "",                  FILENAME,   "edit", 
                                 ES_LEFT | ES_AUTOHSCROLL | WS_BORDER | WS_TABSTOP | WS_CHILD | WS_CLIPCHILDREN,  60, 10,  100,  12
    CONTROL "&Open",             IDOK,       "button",       
                                 BS_DEFPUSHBUTTON | WS_TABSTOP | WS_CHILD | WS_CLIPCHILDREN,  165, 10,  30, 14
    CONTROL "",                  -1,         "static", 
                                 SS_GRAYRECT,  5, 30, 190,  1
    CONTROL "",                  COMPNAME,   "static", 
                                 SS_LEFTNOWORDWRAP | WS_CHILD | WS_CLIPCHILDREN,  5, 36, 190,  8
    CONTROL "Records on file:",     -1,         "static", 
                                 SS_LEFTNOWORDWRAP | WS_CHILD | WS_CLIPCHILDREN,  5, 46,  50,  8
    CONTROL "Accounts:",         -1,         "static", 
                                 SS_LEFTNOWORDWRAP | WS_CHILD | WS_CLIPCHILDREN, 10, 56,  50,  8
    CONTROL "Vendors:",          -1,         "static", 
                                 SS_LEFTNOWORDWRAP | WS_CHILD | WS_CLIPCHILDREN, 10, 66,  50,  8
    CONTROL "Customers:",           -1,         "static", 
                                 SS_LEFTNOWORDWRAP | WS_CHILD | WS_CLIPCHILDREN, 10, 76,  50,  8
    CONTROL "Employees:",           -1,         "static", 
                                 SS_LEFTNOWORDWRAP | WS_CHILD | WS_CLIPCHILDREN, 110, 56,  50,  8
    CONTROL "Inventory:",           -1,         "static", 
                                 SS_LEFTNOWORDWRAP | WS_CHILD | WS_CLIPCHILDREN, 110, 66,  50,  8
    CONTROL "Project:",          -1,         "static", 
                                 SS_LEFTNOWORDWRAP | WS_CHILD | WS_CLIPCHILDREN, 110, 76,  50,  8

    CONTROL "",                  NUMACCOUNTS,         "static", 
                                 SS_LEFTNOWORDWRAP | WS_CHILD | WS_CLIPCHILDREN, 60, 56,  40,  8
    CONTROL "",                  NUMVENDORS,         "static", 
                                 SS_LEFTNOWORDWRAP | WS_CHILD | WS_CLIPCHILDREN, 60, 66,  40,  8
    CONTROL "",                  NUMCUSTOMERS,         "static", 
                                 SS_LEFTNOWORDWRAP | WS_CHILD | WS_CLIPCHILDREN, 60, 76,  40,  8
    CONTROL "",                  NUMEMPLOYEES,         "static", 
                                 SS_LEFTNOWORDWRAP | WS_CHILD | WS_CLIPCHILDREN, 160, 56,  40,  8
    CONTROL "",                  NUMINVENTORY,         "static", 
                                 SS_LEFTNOWORDWRAP | WS_CHILD | WS_CLIPCHILDREN, 160, 66,  40,  8
    CONTROL "",                  NUMPROJECTS,         "static", 
                                 SS_LEFTNOWORDWRAP | WS_CHILD | WS_CLIPCHILDREN, 160, 76,  40,  8

  END


ENTERPASS DIALOG LOADONCALL MOVEABLE DISCARDABLE 50, 45, 130, 55
STYLE DS_MODALFRAME | WS_POPUP | WS_CAPTION | WS_SYSMENU
CAPTION "Sample - Logon"
FONT 8, "MS Sans Serif"
  BEGIN
    CONTROL "User Name :", -1,         "static", 
                           SS_LEFTNOWORDWRAP | WS_CHILD | WS_CLIPCHILDREN | SS_NOPREFIX,  5,  8,  49,  8
    CONTROL "",            USERNAME,   "edit",   
                           ES_LEFT | WS_BORDER | WS_TABSTOP | WS_CHILD | WS_CLIPCHILDREN,  55,  7,  70, 12
    CONTROL "Password :",  -1,         "static",       
                           SS_LEFTNOWORDWRAP | WS_CHILD | WS_CLIPCHILDREN | SS_NOPREFIX,  5, 20,  49,  8
    CONTROL "",            PASSWORD,   "edit",   
                           ES_LEFT | ES_PASSWORD | WS_BORDER | WS_TABSTOP | WS_CHILD | WS_CLIPCHILDREN,  55, 19,  70, 12
    CONTROL "&OK",         IDOK,       "button",       
                           BS_DEFPUSHBUTTON | WS_TABSTOP | WS_CHILD | WS_CLIPCHILDREN,  40, 35,  40, 14
    CONTROL "&Cancel",     IDCANCEL,   "button",       
                           WS_TABSTOP | WS_CHILD | WS_CLIPCHILDREN,  85, 35,  40, 14
  END
