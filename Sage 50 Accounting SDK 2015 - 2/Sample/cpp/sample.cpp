/* ======================================================================================
 * sample.cpp
 *
 * Sample program using the Sage 50 Accounting SDK. The following program will open a
 * Sage 50 Accounting database and display the company information.
 *
 * Copyright (c) 1999-2015 Sage Software Canada, Ltd. All Rights Reserved.
 *
 * ======================================================================================
 */

#include <fcntl.h>
#include <io.h>
#include <objbase.h>
#include <share.h>
#include <stdio.h>
#include <string.h>
#include <windows.h>
#include <windowsx.h>
#include "dbl_define.h"
#include "dbl_general.h"
#include "dbl_return.h"
#include "sample.h"

#import "simply.connectionmanagerservice.tlb" named_guids raw_interfaces_only
#import "simply.connectionmanagerserviceclient.tlb" named_guids raw_interfaces_only

#define  RECBUFFSIZE 2000

static HWND        _hWndMain;
static HINSTANCE   _hInstance;
static char        _szUser[SIZEOF_USERID + 1];
static char        _szPwd[SIZEOF_PASSWORD + 1];
static char			 _szPort[256];
static char			 _szHost[256];

BOOL CALLBACK lSampleDlgProc(HWND, UINT, WPARAM, LPARAM);
BOOL CALLBACK bGetPassDlgProc(HWND, UINT, WPARAM, LPARAM);
void vGetNumRecModule(HWND, WORD, char*, short);
void vDisplayError(HWND, char*);
BOOL bShowSample(HWND, char *);
bool bGetConnectionInfo(LPSTR lpszSimplyFile, LPSTR lpszHost, LPSTR lpszPort, int iHostSize, int iPortSize);
PSTR WINAPI pMakeFileName(PSTR pCompNam,PSTR pBuf,PSTR pExt);


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow)
   {
   MSG   msg;

   // Initialize the thread for apartment-threaded object concurrency.
   _CoInitialize();

   _hInstance = hInstance;
   _hWndMain = CreateDialog(_hInstance, "SAMPLEDLG", NULL, (DLGPROC)MakeProcInstance((FARPROC)lSampleDlgProc, _hInstance));
   if(_hWndMain == NULL)
      return(FALSE);

	ShowWindow(_hWndMain, nCmdShow);
	UpdateWindow(_hWndMain); 

   while (GetMessage(&msg, NULL, 0, 0))
      {
      if (_hWndMain == NULL || !IsDialogMessage (_hWndMain, &msg))
         {
         TranslateMessage(&msg);
         DispatchMessage(&msg);
         }
      }

   // Close the COM library on the current thread, and unload all DLLs loaded by the thread.
   CoUninitialize();

   return((int)msg.wParam);
   }/* WinMain() */


BOOL CALLBACK lSampleDlgProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam)
   {
   char              szFileName[260];
   WIN32_FIND_DATA   sFindData;

   switch (iMessage)
      {
      case WM_COMMAND:
         switch (LOWORD(wParam))
            {
            case IDOK:  
               // see if the file is there
               GetDlgItemText(hDlg, FILENAME, szFileName, sizeof(szFileName));
               if(strlen(szFileName) == 0 || FindFirstFile(szFileName, &sFindData) == INVALID_HANDLE_VALUE)
                  {
                  vDisplayError(hDlg, "The file does not exist. Please select another file.");
                  break;
                  }
               bShowSample(hDlg, szFileName);
            break;
            }
         break;

      case WM_CLOSE:
         _hWndMain = NULL;
         PostQuitMessage(0);
         return(FALSE);
         break;

      default:
         return(FALSE);
      }
   return(TRUE);
   } /* lSampleDlgProc */


BOOL CALLBACK bGetPassDlgProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam)
   {
   switch (iMessage)
      {
      case WM_INITDIALOG:
         memset(_szUser, 0, sizeof(_szUser));
         memset(_szPwd, 0, sizeof(_szPwd));
         break;

      case WM_COMMAND:
         switch (LOWORD(wParam))
            {
            case IDCANCEL:
               EndDialog(hDlg, FALSE);
               break;

            case IDOK:  
					GetWindowText(GetDlgItem(hDlg, USERNAME), _szUser, sizeof(_szUser));
					GetWindowText(GetDlgItem(hDlg, PASSWORD), _szPwd, sizeof(_szPwd));
               EndDialog(hDlg, TRUE);
               break;
            }
         break;

      default:
         return(FALSE);
      }
   return(TRUE);
   } /* bGetPassDlgProc */


BOOL bShowSample(HWND hDlg, char* pszDatabase)
   {
   WORD        wLinkNo,
               wTableNo;
   BYTE        pData[RECBUFFSIZE];
   long        lSNId;
	char		   szCompName[53];
   short       nFileVer,
               nCtyCode, 
               nDBCode;
   bool        bIsAccountantCopyDB;

	char			szDatabaseDir[256];

	pMakeFileName(pszDatabase, szDatabaseDir, ".SAJ");

   //ask for password
   DialogBox(_hInstance, "ENTERPASS", hDlg, (DLGPROC)MakeProcInstance((FARPROC)bGetPassDlgProc, _hInstance));

   //open the database
   if(bGetConnectionInfo(szDatabaseDir, _szHost, _szPort, sizeof(_szHost), sizeof(_szPort)) &&
		wSDBOpenDatabase(&wLinkNo, FLG_READONLY, "Sample", _szUser, _szPwd, _szHost, "", _szPort) == DBS_SUCCESS)
      {

      //check the database version. 
      if(wSDBGetDBVer(wLinkNo, &nFileVer, &nCtyCode, &nDBCode, &bIsAccountantCopyDB) == DBS_SUCCESS)
         {

         if(nFileVer<22201)
            {
            vDisplayError(hDlg, "The SDK will only work with databases created in Sage 50 Accounting (Release 2015.2). Please select another file.");
            wSDBCloseDatabase(wLinkNo);
            vSDBUnloadDAO();
            return(FALSE);
            }
         }

      //open the company info table
      if(wSDBOpenTable(wLinkNo, "tCompany", FLG_READONLY, &wTableNo) == DBS_SUCCESS)
         {
         //read record #1 in company info table, this store the info record
         lSNId = 1;
         memset(pData, 0, RECBUFFSIZE);
         memcpy(pData, &lSNId, 4);
         wSDBGetRecord(wLinkNo, wTableNo, FLG_KEY0, pData);
 
         //get the company name
         memset(szCompName, 0, sizeof(szCompName));

         //changes in 9000 data
         if(nFileVer>9000)
            memcpy(szCompName, pData + 34, 52);
         else
            memcpy(szCompName, pData + 28, 52);

         SetDlgItemText(hDlg, COMPNAME, szCompName);
         wSDBCloseTable(wLinkNo, wTableNo);
         }

      //get the count of records per modules
      vGetNumRecModule(hDlg, wLinkNo, "tAccount", NUMACCOUNTS);
      vGetNumRecModule(hDlg, wLinkNo, "tVendor", NUMVENDORS);
      vGetNumRecModule(hDlg, wLinkNo, "tCustomr", NUMCUSTOMERS);
      vGetNumRecModule(hDlg, wLinkNo, "tEmp", NUMEMPLOYEES);
      vGetNumRecModule(hDlg, wLinkNo, "tInvent", NUMINVENTORY);
      vGetNumRecModule(hDlg, wLinkNo, "tProject", NUMPROJECTS);

      wSDBCloseDatabase(wLinkNo);
      }
   else
      vDisplayError(hDlg, "Cannot open the database. Please select another file.");

   vSDBUnloadDAO();
   return(TRUE);
   } /* bShowSample() */


void vGetNumRecModule(HWND hDlg, WORD wLinkNo, char* pszTableName, short nResId)
   {
   WORD  wTableNo;
   long  lNumRecs;
   char  szNumRec[20];

   if(wSDBOpenTable(wLinkNo, pszTableName, FLG_READONLY, &wTableNo) == DBS_SUCCESS)
      {
      wSDBGetNumRecs(wLinkNo, wTableNo, &lNumRecs);
      _ltoa_s(lNumRecs, szNumRec, 10);
      SetDlgItemText(hDlg, nResId, szNumRec);
      wSDBCloseTable(wLinkNo, wTableNo);
      }
   } /* vGetNumRecModule() */


void vDisplayError(HWND hDlg, char* pszError)
   {
   short       nLoop;

   MessageBox(hDlg, pszError, "Sample - Error", MB_ICONEXCLAMATION | MB_OK |MB_TASKMODAL);
   for(nLoop=COMPNAME; nLoop<=NUMPROJECTS; nLoop++)
      SetDlgItemText(hDlg, nLoop, "");
   } /* vDisplayError() */


//Use COM interop to get the database connection information
bool bGetConnectionInfo(LPSTR lpszSimplyFile, LPSTR lpszHost, LPSTR lpszPort, int iHostSize, int iPortSize)
{
	wchar_t	wPort[260];
	wchar_t	wHost[260];
	wchar_t	wFileNname[260];
	size_t	origsize = strlen(lpszSimplyFile) + 1;
   size_t	convertedChars = 0;
	BSTR		bstrFileName;
	BSTR		bstrPort = SysAllocString(wPort);
	BSTR		bstrHost = SysAllocString(wHost);
	Simply_ConnectionManagerService::ConnectionManagerError err = Simply_ConnectionManagerService::ConnectionManagerError_No_Error;
	Simply_ConnectionManagerServiceClient::IConnectionManagerServiceClientPtr pConnMgrClient;
	HRESULT	hRes;

	//char* to wchar*
   mbstowcs_s(&convertedChars, wFileNname, origsize, lpszSimplyFile, _TRUNCATE);
	bstrFileName = SysAllocString(wFileNname);

	//Start the com interop
	CoInitialize(NULL);     //Initialize all COM Components	
	hRes = pConnMgrClient.CreateInstance(Simply_ConnectionManagerServiceClient::CLSID_ConnectionManagerServiceClient);
	
	if (hRes == S_OK)
	{
	   pConnMgrClient->GetConnectionInfo(bstrFileName, &bstrHost, &bstrPort, &err);
	}
	CoUninitialize ();

	if(err == Simply_ConnectionManagerService::ConnectionManagerError_No_Error)
	{
		convertedChars = 0;
		wcstombs_s(&convertedChars, lpszPort, iPortSize, bstrPort, _TRUNCATE);
		convertedChars = 0;
		wcstombs_s(&convertedChars, lpszHost, iHostSize, bstrHost, _TRUNCATE);
	}

	return (err == Simply_ConnectionManagerService::ConnectionManagerError_No_Error);
}

PSTR WINAPI pMakeFileName(PSTR pCompNam,PSTR pBuf,PSTR pExt)
	{
	int nStrlen;
	PSTR pStr;

	memmove(pBuf,pCompNam,nStrlen=(int)strlen(pCompNam)+1);
	for(pStr = pBuf + nStrlen-1; pStr > pBuf ; pStr--)
		if(*pStr == '.')
			break;

	*pStr = '\0';
	strcat(pStr, pExt);

	return(pBuf);
	}/* pMakeFileName() */
