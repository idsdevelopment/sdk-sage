Option Strict Off
Option Explicit On
Imports System.Runtime.InteropServices
Module Module1
	
	Public Const FLG_SHARED As Short = &H0s
	Public Const FLG_READONLY As Short = &H1s
	Public Const FLG_KEY0 As Short = &H0s
	
	'return codes
	Public Const DBS_SUCCESS As Short = &H0s

   Public Const MAX_PATH As Short = 260
	
	Public Structure FILETIME
		Dim dwLowDateTime As Integer
		Dim dwHighDateTime As Integer
	End Structure
	
	Public Structure WIN32_FIND_DATA
		Dim dwFileAttributes As Integer
		Dim ftCreationTime As FILETIME
		Dim ftLastAccessTime As FILETIME
		Dim ftLastWriteTime As FILETIME
		Dim nFileSizeHigh As Integer
		Dim nFileSizeLow As Integer
		Dim dwReserved0 As Integer
		Dim dwReserved1 As Integer
      <VBFixedString(MAX_PATH), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValArray, SizeConst:=MAX_PATH)> Public cFileName() As Char
      <VBFixedString(14), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValArray, SizeConst:=14)> Public cAlternate() As Char
   End Structure

    Public Declare Function wSDBOpenDatabase Lib "Sage_SA_dblyr.dll" (ByRef pDBLinkNo As Short, ByVal wFlags As Short, ByVal pszAppID As String, ByVal pszUserID As String, ByVal pSZPWD As String, ByVal szHost As String, ByVal szCompId As String, ByVal szPort As String) As Short
    Public Declare Function wSDBCloseDatabase Lib "Sage_SA_dblyr.dll" (ByVal wDBLinkNo As Short) As Short
    Public Declare Function wSDBGetDBVer Lib "Sage_SA_dblyr.dll" (ByVal wDBLinkNo As UShort, ByRef pnFileVer As Short, ByRef pnCtyCode As Short, ByRef pnDBCode As Short, ByRef pnIsAcctCopy As Boolean) As Short

    Public Declare Function wSDBOpenTable Lib "Sage_SA_dblyr.dll" (ByVal wDBLinkNo As Short, ByVal pszTableName As String, ByVal wFlags As Short, ByRef pTBLinkNo As Short) As Short
    Public Declare Function wSDBCloseTable Lib "Sage_SA_dblyr.dll" (ByVal wDBLinkNo As Short, ByVal wTBLinkNo As Short) As Short
    Public Declare Function wSDBGetNumRecs Lib "Sage_SA_dblyr.dll" (ByVal wDBLinkNo As Short, ByVal wTBLinkNo As Short, ByRef pNumRecs As Integer) As Short

    Public Declare Function wSDBGetRecord Lib "Sage_SA_dblyr.dll" (ByVal wDBLinkNo As Short, ByVal wTBLinkNo As Short, ByVal wKey As Short, ByVal pRecord As Byte()) As Short

    Public Declare Sub vSDBUnloadDAO Lib "Sage_SA_dblyr.dll" ()
	
   Public Declare Function FindFirstFile Lib "kernel32" Alias "FindFirstFileA" (ByVal lpFileName As String, ByRef lpFindFileData As WIN32_FIND_DATA) As Integer
End Module