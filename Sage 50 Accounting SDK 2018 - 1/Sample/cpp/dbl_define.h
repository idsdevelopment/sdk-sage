/* ======================================================================================
 * dbl_define.h
 *
 * This file contains miscellaneous defines.
 *
 * Copyright (c) 1999-2014 Sage Software Canada, Ltd. All Rights Reserved.
 * ======================================================================================
 */

//flags for open database
#define FLG_SHARED            0x0000
#define FLG_EXCLUSIVE         0x1010

//Flags used for open table
#define FLG_READWRITE         0x0000
#define FLG_READONLY          0x0001

//flags used for key segments
#define FLG_CURR              0xFFFF
#define FLG_KEY0              0
#define FLG_KEY1              1
#define FLG_KEY2              2
#define FLG_KEY3              3
#define FLG_KEY4              4
#define FLG_KEY5              5
#define FLG_KEY6              6
#define FLG_KEY7              7
#define FLG_KEY8              8
#define FLG_KEY9              9


#define SIZEOF_USERID         8
#define SIZEOF_PASSWORD       8

// Storage size of fixed length fields
#define SIZEOF_DATE           5
#define SIZEOF_TIME           5
#define SIZEOF_REAL           8
#define SIZEOF_INT            2
#define SIZEOF_LONG           4
#define SIZEOF_BOOL           2
